# dup
Program that looks for duplicate lines.

    This code is based on the similarly named program from the book
    "The Go Programming Language" by Alan Donovan, and Brian Kernighan.

**dup usage**
This version looks for duplicate lines from `stdin`

``` bash
$ dup < test.xt
```

**dup2 usage**
This version looks for duplicate lines from a list of files, or from `stdin`.

``` bash
$ dup2 test.txt test2.txt test3.txt

$ dup2 < test.txt
```
